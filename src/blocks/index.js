import Vue from 'vue';

import Header from './header/header';
import AccountIco from './account-ico/account-ico';
import Menu from './menu/menu';
import Preview from './preview/preview';
import About from './about/about';
import Button from './button/button';
import Passes from './passes/passes';
import YouGet from './you-get/you-get';
import WillNot from './will-not/will-not';
import Reviews from './reviews/reviews';
import Modal from './modal/modal';
import Price from './price/price';
import Appeal from './appeal/appeal';
import Footer from './footer/footer';
import Test from './test/test';
import logIn from './log-in/log-in';
import Message from './message/message';


// блоки из одного слова должны иметь постфикс block
Vue.component('header-block', Header);
Vue.component('account-ico', AccountIco);
Vue.component('menu-block', Menu);
Vue.component('preview', Preview);
Vue.component('button-block', Button);
Vue.component('about', About);
Vue.component('passes', Passes);
Vue.component('you-get', YouGet);
Vue.component('will-not', WillNot);
Vue.component('reviews', Reviews);
Vue.component('modal', Modal);
Vue.component('price', Price);
Vue.component('appeal', Appeal);
Vue.component('test', Test);
Vue.component('footer-block', Footer);
Vue.component('log-in', logIn);
Vue.component('message-block', Message);
