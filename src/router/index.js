import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)


import NotFound from "../pages/not-found"
import Home from "../pages/main"
import Policy from "../pages/policy"

const routes = [
  {
    path: '/',
    component: Home,
  },
  {
    path: '/policy',
    component: Policy,
  },
  {
    path: "*",
    component:  NotFound,
  }
];

const router = new VueRouter({
    mode: 'history',
    routes,
});

export default router;
