### Локальный запуск проекта
```bash
npm install
npm run dev
npm run dev -- --host=0.0.0.0
```

### Продовая сборка
```bash
npm run build
```
